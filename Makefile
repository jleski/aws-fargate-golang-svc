VERSION=`git rev-parse HEAD`
BUILD=`date +%FT%T%z`	
LDFLAGS=-ldflags "-X main.Version=${VERSION} -X main.Build=${BUILD}"
IMAGE_TAG='jleskinen/golang-alpine-fargate'

.PHONY: help
help: ## - Show help message
	@printf "\033[32m\xE2\x9c\x93 usage: make [target]\n\n\033[0m"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: build
build:	## - Build the smallest and secured golang docker image based on scratch
	@printf "\033[32m\xE2\x9c\x93 Build the image based on scratch\n\033[0m"
	@docker build -f Dockerfile -t ${IMAGE_TAG}:$(VERSION) .

.PHONY: build-no-cache
build-no-cache:	## - Build the docker image based on scratch with no cache
	@printf "\033[32m\xE2\x9c\x93 Build the docker image\n\033[0m"
	@docker build --no-cache -f Dockerfile -t ${IMAGE_TAG}:$(VERSION) .

.PHONY: ls
ls: ## - List docker images
	@printf "\033[32m\xE2\x9c\x93 Awesome!\n\033[0m"
	@docker image ls ${IMAGE_TAG}

.PHONY: run
run:	## - Run the docker image
	@printf "\033[32m\xE2\x9c\x93 Run the docker image\n\033[0m"
	@docker run -p 9292:9292 ${IMAGE_TAG}:$(VERSION)

.PHONY: push
push:	## - Push docker image to container registry
	$(aws ecr get-login --no-include-email --region eu-central-1)
	@docker push ${IMAGE_TAG}:$(VERSION)

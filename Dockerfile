# golang alpine 1.11.5
FROM golang@sha256:a6435c88400d0d25955ccdea235d2b2bd72650bbab45e102e4ae31a0359dbfb2 as builder
RUN apk update && apk add --no-cache git ca-certificates tzdata && update-ca-certificates && \
    adduser -D -g '' appuser

COPY . .

# Get dependencies and build the binary.
RUN go get -d -v && \
    GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /go/bin/service

FROM scratch

# Import from builder.
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /lib/ld-musl-x86_64.so.1 /lib/ld-musl-x86_64.so.1

# Copy our static executable
COPY --from=builder /go/bin/service /go/bin/service

# Use an unprivileged user.
USER appuser

# Port on which the service will be exposed.
EXPOSE 9292

# Run the service binary.
CMD ["/go/bin/service"]